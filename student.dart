// Name: Sittisal Choolak
// Student ID: 6450110013

void main() {
  Student std = new Student();

  std.name = "Sittisak";
  std.surname = "Choolak";
  std.gpa = 3.99;

  std.display();
}

class Student {
  late String _name, _surname;
  late double _gpa;

  Student(){}

  set name(String n) => _name = n;
  set surname(String s) => _surname = s;
  set gpa(double g) => _gpa = g;

  String get name => _name;
  String get surname => _surname;
  double get gpa => _gpa;

  void display() {
    print("Name: $name $surname\nYour GPA: $gpa");
  }
}